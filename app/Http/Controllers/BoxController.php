<?php

namespace App\Http\Controllers;

use App\Models\Box;
use Illuminate\Http\Request;

class BoxController extends Controller
{
    public function index(Request $request){
        $groupBoxes = Box::select('*');
        if ($request->order != null){
            $groupBoxes  = $groupBoxes->orderBy('colour', 'asc');
        }
        $groupBoxes = $groupBoxes->get()
            ->groupBy('batch')->sort();
        return view('box', compact('groupBoxes'));
    }
}
