<?php

namespace App\Console\Commands;

use App\Events\BoxesGeneratedEvent;
use App\Http\Middleware\PreventRequestsDuringMaintenance;
use App\Models\Box;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class GenerateBox extends Command
{
    private $ColoursArray = ['red', 'yellow', 'green', 'blue', 'pink', 'grey'];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:box';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $boxes       = Box::select('batch', DB::raw('count(*) as boxes'))
            ->groupBy('batch')->pluck('boxes', 'batch')->toArray();
        $length      = count($boxes);
        $boxeslength = $length > 0 ? max($boxes) * 2 : 1;
        $batch       = $length + 1;
        if ($boxeslength <= 16) {
            for ($i = 0; $i < $boxeslength; $i++) {
                $this->store_box_data($batch);
            }
            if ($boxeslength == 16) {
                $data = [
                    'receiver_email' => env('MAIL_TO_ADDRESS')
                ];
                event(new BoxesGeneratedEvent($data));
                return $this->info('email Send');
            } else {
                return $this->info($boxeslength . " Boxes generated");
            }

        }

        return $this->info("boxes generated task end.");
    }

    private function store_box_data($batch)
    {
        $randomColour = array_rand($this->ColoursArray);
        $colour       = $this->ColoursArray[$randomColour];
        $data         = [
            'width'  => '100px',
            'height' => '40px',
            'colour' => $colour,
            'batch'  => $batch
        ];
        Box::create($data);
    }
}
