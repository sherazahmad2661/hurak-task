<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hurak Assignment </title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>
<body>

<div class="container">
    <div class="card mt-5">
        <div class="card-body">
            <div class="border-bottom mb-3">

                <div class="row mb-3">
                    <div class="col-md-8">
                        <p>Colour Array: <code>[red, yellow, green, blue, pink, grey]</code></p>
                    </div>
                    <div class="col-md-4">
                        <a href="{{ route('box.index') }}" class="btn btn-primary">Shuffle Colours</a>
                        <a href="{{ route('box.index', ['order' => true]) }}" class="btn btn-primary">Sort Boxes</a>
                    </div>
                </div>
            </div>

            <div class="d-flex justify-content-start text-center">
                @forelse($groupBoxes as $boxes)
                    <div class="text-white me-3">
                        @forelse($boxes as $box)
                            <div class="mb-2" style="width: {{ $box->width }};
                        height: {{ $box->height }};
                        background-color: {{ $box->colour }}"><h2>{{ $box->colour }}</h2></div>
                        @empty
                        @endforelse
                    </div>
                @empty
                @endforelse
            </div>

        </div>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>

</body>
</html>
